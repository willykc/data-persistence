using UnityEngine;

public class PersistedMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance is null)
            {
                instance = Instantiate(Resources.Load<T>(typeof(T).FullName));
                DontDestroyOnLoad(instance);
            }
            return instance;
        }
    }
}
