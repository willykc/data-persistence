public class GameDataContainer : PersistedMonoBehaviour<GameDataContainer>
{
    public string PlayerName { get; set; }

    public Score BestScore { get; set; }
}
