using System.IO;
using UnityEngine;

public class ScoreService
{
    private static readonly string Path = Application.persistentDataPath + "/bestscore.json";

    private static ScoreService instance;

    public static ScoreService Instance
    {
        get
        {
            if(instance is null)
            {
                instance = new ScoreService();
            }
            return instance;
        }
    }

    private ScoreService() { }

    public void SaveBestScore(Score score)
    {
        string json = JsonUtility.ToJson(score);

        File.WriteAllText(Path, json);
    }

    public Score LoadBestScore()
    {
        if (File.Exists(Path))
        {
            string json = File.ReadAllText(Path);
            var score = JsonUtility.FromJson<Score>(json);
            return score;
        }
        return null;
    }

}
