using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartWindow : MonoBehaviour
{
    [SerializeField]
    private Button playButton;

    [SerializeField]
    private Button quitButton;

    [SerializeField]
    private TMP_InputField nameInput;

    [SerializeField]
    private TextMeshProUGUI bestScore;

    private UnityAction onPlayClick;
    private UnityAction onQuitClick;
    private GameDataContainer gameDataContainer;
    private ScoreService scoreService;

    private void Awake()
    {
        onPlayClick = OnPlayClick;
        onQuitClick = OnQuitClick;
        gameDataContainer = GameDataContainer.Instance;
        scoreService = ScoreService.Instance;
        var score = scoreService.LoadBestScore();
        gameDataContainer.BestScore = score;
        bestScore.text = score is null ? "No Best Score Yet" : $"Best Score : {score.name} : {score.score}";
    }

    private void OnEnable()
    {
        playButton.onClick.AddListener(onPlayClick);
        quitButton.onClick.AddListener(onQuitClick);
    }
    private void OnDisable()
    {
        playButton.onClick.RemoveListener(onPlayClick);
        quitButton.onClick.RemoveListener(onQuitClick);
    }

    private void OnPlayClick()
    {
        gameDataContainer.PlayerName = nameInput.text;
        SceneManager.LoadScene("main");
    }

    private void OnQuitClick()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
